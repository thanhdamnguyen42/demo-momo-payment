const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const https = require('https');


/* LƯU ENV */
const partnerCode = "MOMO";
const accessKey = "F8BBA842ECF85";
const secretkey = "K951B6PE1waDMi640xX08PD3vg6EkVlz";
// url khi thanh toan xong
const redirectUrl = "http://139.180.213.206/return";
// url trả về data của momo
const ipnUrl = "http://139.180.213.206/inp_momo";



/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Demo Payment Momo' });
});


router.post('/payment-momo', async (req, res, next) => {
  try {
    /*
      * device mặc định là web
      * là app nếu sd = SDK
     */
    const { amount = '10000', device = 'web' } = req.query
    const requestId = partnerCode + new Date().getTime();
    const orderId = requestId;

    const orderInfo = "Thanh Toan Askany";
    const requestType = "captureWallet"
    const extraData = "";

    const rawSignature = "accessKey=" + accessKey + "&amount=" + amount + "&extraData=" + extraData + "&ipnUrl=" + ipnUrl + "&orderId=" + orderId + "&orderInfo=" + orderInfo + "&partnerCode=" + partnerCode + "&redirectUrl=" + redirectUrl + "&requestId=" + requestId + "&requestType=" + requestType
    //signature
    const signature = crypto.createHmac('sha256', secretkey)
      .update(rawSignature)
      .digest('hex');
    console.log("--------------------SIGNATURE----------------")

    const requestBody = JSON.stringify({
      partnerCode: partnerCode,
      accessKey: accessKey,
      requestId: requestId,
      amount: amount,
      orderId: orderId,
      orderInfo: orderInfo,
      redirectUrl: redirectUrl,
      ipnUrl: ipnUrl,
      extraData: extraData,
      requestType: requestType,
      signature: signature,
      lang: 'vi'
    });

    //Create the HTTPS objects
    const options = {
      hostname: 'test-payment.momo.vn',
      port: 443,
      path: '/v2/gateway/api/create',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'Content-Length': Buffer.byteLength(requestBody)
      }
    }


    let endPoint = ''
    const request = https.request(options, (response) => {
      console.log(`Status: ${response.statusCode}`);
      response.setEncoding('utf8');
      response.on('data', (body) => {
        endPoint = JSON.parse(body).payUrl
      });

      response.on('end', () => { })
    })


    request.on('error', (e) => {
      console.log(`problem with request: ${e.message}`);
    });
    // write data to request body
    console.log("Sending....")
    request.write(requestBody);
    request.end();



    setTimeout(async () => {
      if (device == 'app')
        res.status(200).json({ data: { orderId, amount, requestId } })
      else
        res.redirect(endPoint)
    }, 1000)

  } catch (error) {
    console.log(error.message)
  }
})

router.get('/return', function (req, res, next) {
  try {
    const data = req.query

    // đá về page muốn đá khi thanh toán xong
    if (data.resultCode == 0)
      res.status(200).send('========================== Thanh Toan Thanh Cong ==========================')
    else res.status(400).send('========================== Thanh Toan That Bai ==========================')


  } catch (error) {
    console.log(error.message)
  }
});

router.post('/inp_momo', (req, res, next) => {
  try {
    console.log('========================== inp_momo ==========================')
    const data = req.body

    /* Xử lý gói data nếu thành công */
    if (data.resultCode == 0) {
      // Thành Công
      console.log({ dataInp: data })

      res.status(400).json({ error: true, message: 'success' })

    } else {
      res.status(400).json({ error: true, message: 'error' })
    }

  } catch (error) {
    console.log(error.message)
  }
})

module.exports = router;
